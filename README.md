# FTP-Server

The File Transfer Protocol (FTP) is a standard communication protocol used for the transfer of computer files from a server to a client on a computer network. 
In this project, we implemented an FTP Server on a client–server model architecture using separate control and data connections between the client and the server.

## Developers

* [**Ghazal Kalhor**](https://github.com/kalhorghazal)
* [**Amin Baqershahi**](https://github.com/aminb7)
